#include "headers.h"

float Median(float x[], int elem){
    float res = 0.0;
    float temp = 0.0;  
    int minPos = 0;
    int j = 0;     
    int i = 0;
    
    for(i=0;i<elem-1;i++){
        minPos = i;
        for(j=i+1;j<elem;j++){
            if(x[j] < x[minPos]){
                minPos = j;
            }
        }
        temp=x[i];
        x[i]=x[minPos];
        x[minPos]=temp;
    }

    if(elem % 2 == 0){
        res = (x[(elem/2)] + x[(elem/2)-1])/2;
    }else{
        res = x[(elem/2)];
    }
    
    return res;
}