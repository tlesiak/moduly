CC=gcc
CFLAGS=-Wall #flaga umożliwiająca wyświetlenie ostrzeżeń
LIBS=-lm #jeśli chcemy używać math.h

main: main.o mean.o sd.o median.o #aby otrzymać main potrzeba main.o i mean.o
	$(CC)	$(CFLAGS) -o main main.o mean.o sd.o median.o $(LIBS)

main.o: main.c headers.h #aby otrzymać main.o potrzeba main.c i fun.h
	$(CC)	$(CFLAGS) -c main.c

mean.o: mean.c #aby otrzymać main potrzeba mean.c
	$(CC)	$(CFLAGS) -c mean.c

sd.o: sd.c #aby otrzymać main potrzeba mean.c
	$(CC)	$(CFLAGS) -c sd.c

median.o: median.c #aby otrzymać main potrzeba mean.c
	$(CC)	$(CFLAGS) -c median.c